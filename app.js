
var express = require('express');
var bodyParser = require('body-parser');
var requestJson = require('request-json');
var dateFormat = require('dateformat');
var uuidv4 = require('uuid/v4');
var app = express();
var port = process.argv.PORT || 3000;


var baseMlabURL = "https://api.mlab.com/api/1/databases/mi_banckito/collections/";
var API_KEY = "apiKey=Q-EdvvCEXq_wxzntaMTr6fgzpHaoH339";

var URLbase='/api/v1/';



app.use(bodyParser.json());

//npm install cors --save
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  
  app.post(URLbase + 'user', function(req, res){
    console.log("GET /api/v1/user");
    let newUser = {
        "user_id": uuidv4(),
        "first_name": req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "document_number" : req.body.document_number,
        "password": "123456",
        "document_type": "DNI",
        "accounts": [
            {
                "number": uuidv4(),
                "type": "Cuenta Fácil"
            },
        ],
      };
    clienteMlab = requestJson.createClient(baseMlabURL);
    clienteMlab.post("user?"+ API_KEY, newUser ,function(err, resM, body){
        response ={"msg": "Usuario creado correctamente",newUser};
        res.status(200).send(response);
   });
});

app.post(URLbase + 'login', function(req, res){
    console.log("POST /api/v1/login");
    let document_number = req.body.documentNumber;
    let password = req.body.password;
    console.log("dato recibidos " + document_number + "::" + password);
    let query = 'q={"document_number":"'+document_number+'","password":"'+password+'"}&';
    clienteMlab = requestJson.createClient(baseMlabURL);
    clienteMlab.get('user?'+ query + API_KEY , function(err, resM, body) {
        if (!err) {
            if(body.length>=1){
                var query_1= '?q={"id": ' + body[0]._id.$oid + '}&'
                var cambio = '{"$set":{"logged":true}}';
                clienteMlab.put("user?"+query_1+ API_KEY,JSON.parse(cambio),function(errP,resP,bodyP){
                    console.log(bodyP.n);
                    var response ={"msg": "login correcto" , "user":body};
                    res.status(200).send(response);
                });
            }
            else{
                response ={"msg": "Usuario no encontrado"};
                res.status(404).send(response);
            }
        }
    });
});
app.get(URLbase + 'movement/:id', function(req, res){
    console.log("GET /api/v1/movement");
    let account_id = req.params.id;
    console.log(account_id);
    const query = 'q={"account_id": "'+account_id+'"}&';
    clienteMlab = requestJson.createClient(baseMlabURL);
    clienteMlab.get('movement?'+ query+ API_KEY , function(err, resM, body) {
        if (!err) {
            console.log(body.length);
            res.status(200).send(body);
            
        }
        else 
        {
            console.log("Error: " + err);
        }
    });
});

app.post(URLbase + 'movement', function(req, res){
    console.log("POST /api/v1/movement");
    let movementIn= {
        "account_id" :  req.body.accountNumberDestination,
        "amount": "$" +req.body.amount,
        "description" : "Depósito de otra cuenta",
        "date" : dateFormat(new Date(), "dd-mm-yyyy h:MM:ss"),
        "location_latitude" : "-12.0940664",
        "location_longitude" : "-77.021351",
        "type": "+"
    };
    clienteMlab = requestJson.createClient(baseMlabURL);
    clienteMlab.post("movement?"+ API_KEY, movementIn ,function(err, resM, body){
        let movementOut = {
            "account_id" :  req.body.accountNumberSource,
            "amount": "$" +req.body.amount,
            "description" : "Tranferencia a otra cuenta",
            "date" : dateFormat(new Date(), "dd-mm-yyyy h:MM:ss"),
            "location_latitude" : "-12.0940664",
            "location_longitude" : "-77.021351",
            "type": "-"
        };
        clienteMlab.post("movement?"+ API_KEY, movementOut ,function(err, resM, bodyT){
            
            response ={"msg": "true"};
            res.status(200).send(response);
        });
    });
});


app.get(URLbase + 'balancebyuser/:id', function(req, res){
    console.log("GET /api/v1/balancebyuser");
    let user_id = req.params.id;
    console.log("USUARIO: " +user_id);
    const query = 'q={"user_id": "' + user_id + '"}&';
    clienteMlab = requestJson.createClient(baseMlabURL);
    clienteMlab.get('user?'+ query + API_KEY , function(err, resM, body) {
        if (!err) {
            let response_array = new Array();
            body[0]["accounts"].forEach(element => {          
                let totalIngresos=0;
                let totalEgresos=0;
                clienteMlab = requestJson.createClient(baseMlabURL);
                const query = 'q={"account_id":"'+element.number+'"}&';
                var sync = true;
                clienteMlab.get('movement?'+ query+ API_KEY , function(err, resM, bodymov) {
                    console.log("llega");
                    bodymov.forEach(element => {
                        let amount=parseFloat(element["amount"].split("$")[1]);
                        if(element["type"]==="+"){
                            totalIngresos=totalIngresos+amount;
                        }
                        else{
                            totalEgresos=totalEgresos+amount;
                        }
                    });
                    var obj = {
                        "ingresos": totalIngresos,
                        "egresos": totalEgresos,
                        "cuenta_tipo": element.type,
                        "cuenta_numero": element.number,
                    }
                    response_array.push(obj);
                    sync = false;
                });
                while(sync){require('deasync').sleep(100);}
                
            });
            console.log("antes del response");
            console.log(response_array)
            res.status(200).send({"data":response_array});
        }
    });
});

app.get(URLbase + 'account/:id', function(req, res){
    console.log("GET /api/v1/account");
    let user_id = req.params.id;
    console.log(user_id);
    const query = 'q={"user_id": "' + user_id + '"}&';
    
    clienteMlab = requestJson.createClient(baseMlabURL);
    clienteMlab.get('user?'+ query + API_KEY , function(err, resM, body) {
        if (!err) {
            let response_array = new Array();
            body[0]["accounts"].forEach(element => {          
                let totalIngresos=0;
                let totalEgresos=0;
                clienteMlab = requestJson.createClient(baseMlabURL);
                const query = 'q={"account_id":"'+element.number+'"}&';
                var sync = true;
                clienteMlab.get('movement?'+ query+ API_KEY , function(err, resM, bodymov) {
                    console.log("llega");
                    bodymov.forEach(element => {
                        let amount=parseFloat(element["amount"].split("$")[1]);
                        if(element["type"]==="+"){
                            totalIngresos=totalIngresos+amount;
                        }
                        else{
                            totalEgresos=totalEgresos+amount;
                        }
                    });
                    var obj = {
                        "ingresos": totalIngresos,
                        "egresos": totalEgresos,
                        "cuenta_tipo": element.type,
                        "cuenta_numero": element.number,
                        "saldo": (totalIngresos-totalEgresos).toFixed(2)
                    }
                    response_array.push(obj);
                    sync = false;
                });
                while(sync){require('deasync').sleep(100);}
                
            });
            res.status(200).send(response_array);
            
        }
        else 
        {
            console.log("Error: " + err);
        }
    });
});

app.listen(port);