
echo "################## Construyendo imagen docker ##################"
docker build -t cecum/myapp .
echo "################## Tag de la imagen docker ##################"
docker tag cecum/myapp:latest 110235682872.dkr.ecr.us-west-2.amazonaws.com/mibankito:latest
echo "################## Subiendo a AWS ECS repository ##################"
$(aws ecr get-login --no-include-email --region us-east-1)
docker push 110235682872.dkr.ecr.us-west-2.amazonaws.com/mibankito:latest
echo "################## Registrando definicion de tarea ##################"
aws ecs register-task-definition --cli-input-json file://aws-task-def.json
echo "################## ejecutando tarea ##################"
#aws ecs run-task --task-definition hello-world
aws ecs update-service --cluster mibankito-cluster --service mibankito-service --force-new-deployment